import turtle
from collections import deque

def paintGraph(t):
	l=deque()
	lun = 500.0
	pos=t.pos()
	l.appendleft((pos[0],pos[1],lun))
	while l:
		curent=l.pop()
		setMyTurtle(t,curent)
		drawSquare(t,curent[2])
		rat=curent[2]/20
		nlung=curent[2]/2-rat
		addCurentState(l,t,nlung,rat/2)

def drawSquare(t,lun):
	for i in xrange(4):
		t.forward(lun)
		t.left(90)

def moveMyT(t):
	t.penup()
	setMyTurtle(t,curent)
	t.pendown()


def setMyTurtle(t,curent):
	t.penup()
	t.goto(curent[0],curent[1])
	t.pendown()

def addCurentState(l,t,lun,rat):
	pos=t.pos()
	l.appendleft((pos[0]+rat,pos[1]+rat,lun))
	l.appendleft((pos[0]+rat,pos[1]+lun+rat*3,lun))
	l.appendleft((pos[0]+lun+3*rat,pos[1]+rat,lun))
	l.appendleft((pos[0]+lun+3*rat,pos[1]+lun+rat*3,lun))


t=turtle.Turtle()
t.penup()
t.goto(-300,-300)
t.pendown()
t.speed(0)
paintGraph(t)

