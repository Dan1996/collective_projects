import sys
import time

def fill(matrix,pornire):
    '''

    :param matrix:
    :param pornire: linie , coloana
    :return:
    '''
    visitedMatrix=makeFullMatrix(False)
    visitedMatrix[pornire[0]][pornire[1]]=True
    nevastaLuSteve=[]
    nevastaLuSteve.append(pornire)
    while nevastaLuSteve:
        curent=nevastaLuSteve[-1]
        nevastaLuSteve.pop()
        matrix[curent[0]][curent[1]] = 'x'
        adaugaSteve(nevastaLuSteve,curent,matrix,visitedMatrix)
        printMatrix(matrix)
        print
        time.sleep(0.1)


def isInRange(cell,nr):
    if cell[0] < 0 or cell[0] == nr:
        return False
    if cell[1] < 0 or cell[1] == nr:
        return False
    return True


def valid(matrix,visitedMatrix,cell):
    if isInRange(cell,len(matrix)) and visitedMatrix[cell[0]][cell[1]]==False and matrix[cell[0]][cell[1]] != '#':
        return True
    return False

def adaugaSteve(nevastaLuSteve,curent,matrix,visitedMatrix):
    selected=(curent[0] - 1, curent[1])
    if valid(matrix,visitedMatrix,selected):
        visitedMatrix[selected[0]][selected[1]]=True
        nevastaLuSteve.append(selected)
    selected=(curent[0]+1,curent[1])
    if valid(matrix,visitedMatrix,selected):
        visitedMatrix[selected[0]][selected[1]]=True
        nevastaLuSteve.append(selected)
    selected=(curent[0],curent[1]-1)
    if valid(matrix,visitedMatrix,selected):
        visitedMatrix[selected[0]][selected[1]] = True
        nevastaLuSteve.append(selected)
    selected=(curent[0],curent[1]+1)
    if valid(matrix,visitedMatrix,selected):
        visitedMatrix[selected[0]][selected[1]] = True
        nevastaLuSteve.append(selected)

def makeFullMatrix(el):
    matrix=[[el for i in xrange(30)] for j in xrange(30)]
    return matrix

def printMatrix(matrix):
    for line in matrix:
        for el in line:
            print el,
        print

def addSquare(matrix,x,y,lung):
    for i in xrange(lung):
        matrix[y][x+i]='#'
        matrix[y+i][x] = '#'
    y=y+lung-1
    for i in xrange(lung):
        matrix[y][x-i] = '#'
        matrix[y-i][x-lung]='#'
    x=x-lung
    y=y-lung+1
    matrix[y-1][x]='#'
    matrix[y-2][x]='#'
    y=y-2
    for i in xrange(2*lung):
        matrix[y][x+i]='#'
    matrix[y+1][x+2*lung-1]='#'

        #matrix[y+lung-1][x+i] = '#'

def myMain(selected):
    matrix=makeFullMatrix('.')
    addSquare(matrix,15,15,7)
    if matrix[selected[0]][selected[1]]=="#":
        print 'baiete , punctu tau ii in perete'
        return
    fill(matrix,selected)

def testFill():
    pass

myMain((int(sys.argv[1]),int(sys.argv[2])))