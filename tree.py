import os
import sys

def tree(path,space):
    files=os.listdir(path)
    lf = [el  for el in files if os.path.isfile(os.path.join(path, el))]
    ld = [el  for el in files if not os.path.isfile(os.path.join(path, el))]
    for dir in ld:
        print format(dir,space)
        tree(os.path.join(path, dir), space + 1)
    for file in lf:
        print format(file,space)

def format(file,space):
    myst=''
    for i in xrange(space):
        myst+='|'+' '*3
    myst=myst[:-2]+'-'+' '
    return myst+file

tree(sys.argv[1],1)

