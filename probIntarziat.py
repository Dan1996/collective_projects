

def binarySearch(grid,el):
    a=0
    b=len(grid)-1
    while a<=b:
        m=(a+b)//2
        if grid[m]==el:
            return m
        elif grid[m]<el:
            a=m+1
        else:
            b=m-1

def intarziatGrid(grid,el):
    #naive aproach
    for i in xrange(len(grid)):
        searched=binarySearch(grid[i],el)
        if searched!=None:
            return i,searched

def tesIntarziatGrid():
    assert binarySearch([3,6,9],9)==2
    assert binarySearch([1,5,7,9,10,11,12,13,14],0)==None
    assert intarziatGrid([[1,4,6],[2,5,8],[3,6,9]],9)==(2,2)

tesIntarziatGrid()