import time

#negru 0-255 alb
def rescale(f, fm, fM, tm, tM):
    f = float(f)
    fM-=fm
    f-=fm
    rap=f/fM
    tM-=tm
    f=tM*rap+tm
    return f

def drawBtoW(f):
    for pixY in xrange(2024):
        ry = rescale(pixY, 0, 2023, 0, 127)
        for pixX in xrange(2024):
            rx = rescale(pixX, 0, 2023, ry, ry + 127)
            f.write(chr(int(rx)))


def drawBW(f):
    for pixY in xrange(2024):
        for pixX in xrange(2024):
            decider=(pixX+pixY)//90
            if int(decider)%2==0:
                f.write(chr(255))
            else:
                f.write(chr(0))
        f.write('\n')


f=open('pozaGri','w')
f.write('P5\n')
f.write('2024 2024\n')
f.write('255\n')
drawBtoW(f)


