# exemplu
# scrie un program care afiseaza imaginea
# o . . . .
# o o . . .
# o o o . .
# o o o o .
# o o o o o

def pattern1():
    pass

# . . o o . . o o .
# . . o o . . o o .
# o o . . o o . . o
# o o . . o o . . o
# . . o o . . o o .
# . . o o . . o o .
# o o . . o o . . o
# o o . . o o . . o
# . . o o . . o o .
def pattern_check():
    N = 9
    for i in range(N):
        for j in range(N):
            if (i % 4 < 2) != (j % 4 < 2):
                print 'o',
            else:
                print '.',
        print

# --- py.test code below ---

def test_pattern1(capsys):
    pattern1()
    out, err = capsys.readouterr()
    assert out == '''\
o . . . .
o o . . .
o o o . .
o o o o .
o o o o o
'''


