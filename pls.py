import os
import sys

def meml(dim):
	KB=dim/1024
	MB=KB/1024
	if MB>0:
		return str(MB)+','+str(KB)[0]+'m'
	elif KB>0:
		return str(KB)+','+str(dim)[0]+'k'
	return str(dim)+' '+'b'

def spacing(col,el):
	return ' ' * (col - len(str(el)))

def format(el ,myt, nr,col1,col2,col3,color):
	line=colorText(93)+'| '+colorText(color)+el+spacing(col1,el)+spacing(col2,myt)+str(myt)+spacing(col3,nr)+str(nr)
	return line

def myMax(l,n):
	myMaxEx=0
	for el in l:
		if len(el[n]) > myMaxEx:
			myMaxEx = len(el[n])
	return myMaxEx

def sizeFullPath(curent,el):
	return os.path.getsize(os.path.join(curent,el))

def numberOfFiles(curent,el):
	return str(len(os.listdir(os.path.join(curent,el))))

def colorText(col):
	return '\x1b['+str(col)+'m'

def printFiles(curent):
	l=os.listdir(curent)
	l.sort()
	lf=[(el,os.path.splitext(el)[1],meml(sizeFullPath(curent,el))) for el in l if os.path.isfile(os.path.join(curent,el))]
	ld=[(el,numberOfFiles(curent,el),'items') for el in l if not os.path.isfile(os.path.join(curent,el))]
	lc=lf+ld
	maxName=myMax(lc,0)+1
	maxType=myMax(lc,1)+1
	maxSz=myMax(lc,2)+1
	for el in ld:
		color=94
		if el[0][0]=='.':
			color=90
		print format(el[0],el[1],el[2],maxName,maxType,maxSz,color)
	for el in lf:
		color=33
		if el[1]=='.py':
			color=32
		print format(el[0],el[1],el[2],maxName,maxType,maxSz+1,color)
	print colorText(0)

try:
	myP=sys.argv[1]
except IndexError:
	myP='.'
printFiles(myP)
