"""
primeste o lista de tuplete(time,cmd)
returneaza o lista ordonata invers dupa numarul aparitilor comenzilor
"""
import parse_file


def statistics(list):
    dict = {}
    for time, cmd in list:
        if cmd not in dict:
            dict[cmd] = 0
        dict[cmd] += 1
    rez = dict.items()
    return sorted(rez, key=lambda x: x[1], reverse=True)


def test_statistics():
    assert statistics(parse_file.parse_file("test1_statistics.txt")) == [('pip', 3), ('ls', 3), ('cd', 2), ('rm', 1),
                                                                         ('vim', 1)]
    assert statistics(parse_file.parse_file("test2_statistics.txt")) == [('yolk', 7), ('yum', 3), ('pip', 3),
                                                                         ('sudo', 2),
                                                                         ('svn', 1), ('vim', 1), ('tail', 1), ('ls', 1)]
    try:
        statistics(parse_file.parse_file("inexistent.txt"))
        assert False
    except IOError:
        assert True
