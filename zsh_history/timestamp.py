from datetime import datetime

def get_time_stamp(path):
    """Parse a zsh history file and return date and command"""
    return_list = []
    try :
        file_r = open(path,"r")
        for line in file_r :
            try :
                #Obtain timers
                timers = line.split(";")[0].split(":")[1].replace(" ","")
                #timers_formatted = datetime.fromtimestamp(int(timers)).strftime('%Y-%m-%d')
                timers_formatted = timers
                #Obtain commands
                comm = line.split(";")[1].split()[0]
            except IndexError :
                pass
            #For every line append tuple of timers and commands
            return_list.append((timers_formatted,comm))
    except (OSError,IOError) as e :
        print str(e)
    return return_list

f = get_time_stamp(".zsh_history_sanitized")
print f


# def diff_between_dates(date1,date2):
#     """ obtain diffrence between two dates"""
#     date1_formatted = datetime.strptime(date1, "%Y-%m-%d")
#     date2_formatted = datetime.strptime(date2, "%Y-%m-%d")
#     return abs((date1_formatted - date2_formatted).days)
#
# d = diff_between_dates(f[0][0],f[len(f)-1][0])
# print "Diffrence between two dates (first and last): " + str(d)
#
# def obtain_timelapse_intervals(number_of_days,splits):
#     intervals = number_of_days/splits
#     return intervals
#
# o = obtain_timelapse_intervals(d,8)
# print "Number of days between intervals : " + str(o)

def calculate_timestamps(parsed_text):
    list_of_times= []
    steps = len(parsed_text)/8
    for i in xrange(0,len(parsed_text),steps) :
        list_of_times.append(datetime.fromtimestamp(int(parsed_text[i][0])).strftime('%Y-%m-%d'))
    return list_of_times

print "Times : " + str(calculate_timestamps(f))
# def print_intervals(parsed_text,intervals):
#     get_unqiue_dates = set(el[0] for el in parsed_text)
#
#     for i in xrange(0,len(parsed_text),intervals):
#             print parsed_text[i][0]

#print print_intervals(f,o)




