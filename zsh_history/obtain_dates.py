from datetime import datetime
import timestamp

def calculate_timestamps(parsed_text):
    """ Helper method for obtaining and displaying date intvervals """
    list_of_times= []
    #Obtain number of timestamps between to dates
    steps = len(parsed_text)/8
    for i in xrange(0,len(parsed_text),steps) :
        #Append to the list the timestamp converted
        list_of_times.append(datetime.fromtimestamp(int(parsed_text[i][0])).strftime('%Y-%m-%d'))
    return list_of_times

f = timestamp.get_time_stamp(".zsh_history_sanitized")

print "Times : " + str(calculate_timestamps(f))
