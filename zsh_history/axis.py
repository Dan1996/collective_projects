import turtle
import timestamp
import obtain_dates
from datetime import datetime

# rescale transforms a number from the range fm..fM to the tm..tM range
# This is the generalization of the color use case
# but now f ranges not from 0..255 but from fm..fM
def rescale(f, fm, fM, tm, tM):
    f = float(f)
    fM-=fm
    f-=fm
    rap=f/fM
    tM-=tm
    f=tM*rap+tm
    return f

def maxName(mostUsedDict):
    max = 0
    for el in mostUsedDict.items():
        if (len(el[0]) > max):
            maxEl = len(el[0])
    return maxEl

def myShow(turtle,message,x,y):
    '''
    :param turtle: obiect de tip turtle
    :param message: string ce trebuie afisat
    :param x: coordonata x
    :param y: coordonata y
    afiseaza un mesaj la coordonatele x,y in spatiu turtle
    '''
    turtle.penup()
    turtle.goto(x,y)
    turtle.write(message)
    turtle.pendown()

def drawYAxis(timeComandList,turtle,myXrange,myYrange,err):
    '''
    :param timeComandList: lista de
    :param turtle: obiect de tip turtle
    :param myXrange: lungimea axei OX
    :param myYrange: lungimea axei OY
    :param err: eroare cu care trebuie deplasata axa OX
    functia afiseaza axa OY formata din comenzile primite
    '''
    inRangeY = myYrange / 2
    for i, el in enumerate(timeComandList):
        y = rescale(i+1 , 0, len(timeComandList), -inRangeY, inRangeY)
        myShow(turtle, el[0], -myXrange + err, y)

def drawXAxis(timeComandList,turtle,myXrange,myYrange,err):
    '''
    :param timeComandList: lista de
    :param turtle: obiect de tip turtle
    :param myXrange: lungimea axei OX
    :param myYrange: lungimea axei OY
    :param err: eroare cu care trebuie deplasata axa OX
    functia afiseaza axa OX formata in comenzile primite
    '''
    parsed_text = timestamp.get_time_stamp(".zsh_history_sanitized")
    datesIntervals=obtain_dates.calculate_timestamps(parsed_text)
    err+=150
    myYrange/=2
    divide=8
    x=-myXrange+err
    actualRangeX=int(parsed_text[0][0])
    actualRangeY=int(parsed_text[-1][0])
    rat=(actualRangeY-actualRangeX)//divide
    step=actualRangeX
    for i in xrange(divide-1):
        lx=x
        x=rescale(step,actualRangeX,actualRangeY,-myXrange+err,myXrange)
        myShow(turtle, datesIntervals[i], (x+lx)/2  ,-myYrange)
        step += rat
    lx=x
    x = rescale(step, 0,int(parsed_text[-1][0]) , -myXrange+err, myXrange)
    myShow(turtle, datesIntervals[len(datesIntervals)-1], (x+lx)/2, -myYrange)

def printAxis(turtle,timeComandList,mostUsedDict):
    '''
    turtle-un obiect de tip turtle
    timeComandList-o lista de tupluri de tip (timeLine,comanda)
    mostUsedDict-dictionar cu elemente de tip comanda:cateUtilizari
    functia afiseaza coordonatele x,y in functie de lungimea ecranului
    '''
    myXrange=turtle.window_width()
    myYrange=turtle.window_height()
    myElements=sorted(mostUsedDict.items(),key=lambda x:x[1])
    err=maxName(mostUsedDict)#+300
    drawYAxis(myElements,turtle,myXrange,myYrange,err)
    drawXAxis(timeComandList,turtle,myXrange,myYrange,err)
    scatterPlot(turtle,myElements,timeComandList,myXrange,myYrange,err)

def drawDot(x,y):
    turtle.penup()
    turtle.goto(x,y)
    turtle.dot()
    turtle.penup()

def folosit(timeComandList,element,date):
    for index,el in enumerate(timeComandList):
        if int(el[0])<=date and el[1]==element[0]:
		timeComandList.pop(index)
		return True
    return False

def scatterPlot(turtle,myElements,timeComandList,xRange,yRange,err):
    parsed_text = timestamp.get_time_stamp(".zsh_history_sanitized")
    actualRangeX=int(parsed_text[0][0])
    actualRangeY=int(parsed_text[-1][0])
    err+=150
    step=3600*24
    for i in xrange(actualRangeX,actualRangeY,step):
        x=rescale(i,actualRangeX,actualRangeY,-xRange+err,xRange)
        for j in xrange(len(myElements)):
	    if folosit(timeComandList,myElements[j],i):
            	y=rescale(j+1,0,len(myElements),-yRange/2,yRange/2)
	    	drawDot(x,y)
            

def most_used_commands(parsed_file):
    commands_dict = {}
    for command in parsed_file:
        try :
            commands_dict[command[1]] = commands_dict[command[1]] + 1
        except KeyError :
            commands_dict[command[1]] = 1
    maxCommandsList = sorted(commands_dict.items(),key=lambda x: x[1],reverse=True)[:50]
    return dict(maxCommandsList)



def testMyFunc():
    t=turtle.Turtle()
    t.speed(10)
    timeComandList=timestamp.get_time_stamp(".zsh_history_sanitized")
    mostUsedDict= most_used_commands(timeComandList)
    parsed_text = timestamp.get_time_stamp(".zsh_history_sanitized")
    printAxis(t,timeComandList,mostUsedDict)
    #turtle.done()

testMyFunc()
