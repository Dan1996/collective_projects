def makeListPaths(pathList):
    splitPaths = []
    for path in pathList:
        splitPaths.append(path.split('/'))
    return splitPaths

def addDictPath(dict,path):
    for file in path:
        if file not in dict.keys():
            dict[file]={}
        dict=dict[file]

def un_flatten(pathList):
    dictPath={}
    splitPaths=makeListPaths(pathList)
    for path in splitPaths:
        addDictPath(dictPath,path)
    return dictPath


def test_un_flatten():
    tree= un_flatten(['A/B/T','A/U','A/U/Z'])
    assert tree=={
        'A':{
            'B':{'T':{}},
            'U':{
                'Z':{}
            }
        }
    }



print un_flatten(['A/B/T','A/U','A/U/Z'])