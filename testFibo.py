from fibo import fiboRec
from fiboIt import fiboIt
from contextlib import redirect_stdout

def testFibo():
	with open('help.txt','w') as f:
		with redirect_stdout(f):
			fiboRec(2,1,1)
			fiboIt(100)
	
