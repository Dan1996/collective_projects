"""
Ansii escape codes are magic strings that the terminal interprets as commands
Some of the most ab-used ones change the color of the text
They begin with the character 27 = 0x1b.
Python strings can contain hex escapes so chr(27) == '\x1b'
The syntax of ansii escapes is
\x1b[21m
\x1b[34m
etc. In general \x1b[numberm
"""

def toesc(c):
    """
    Returns the ansii escape with the code c
    \x1b[cm
    """
    return '\033[{}m'.format(c)

# codes from 90 to 98 and from 30 to 38 change the text's color
# code zero 0 resets the terminal to the default state

# define the reset code
ENDC = toesc(0)
# define the following colors corresponding to the code range 90 98
LBLACK, LRED, LGREEN, LYELLOW, LBLUE, LPURPLE, LCYAN, LWHITE = [toesc(x) for x in xrange(90, 98)]
# define the following colors corresponding to the code range 30 38
DBLACK, DRED, DGREEN, DYELLOW, DBLUE, DPURPLE, DCYAN, DWHITE = [toesc(x) for x in xrange(30, 38)]

def colored_text(text, color=ENDC):
    """ returns the text surrounded by ansii escapes that would make it print in the given color on a terminal """
    return '{}{}{}'.format(color, text, ENDC)


def welcome_python():
    """ prints Python welcomes the terminal. Each work in a different color"""
    print colored_text("Python", DBLUE),
    print colored_text("welcomes", DYELLOW),
    print colored_text("the", LBLUE),
    print colored_text("terminal", DRED)

welcome_python()

# using sys.argv os.listdir and os.path.join and os.path.isdir
# implement a version of ls that prints directories blue and files normally

