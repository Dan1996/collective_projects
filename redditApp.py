import json
import urllib2
import xml.etree.cElementTree as ET

def printCommentsJson(myUrl):
    link=urllib2.urlopen(myUrl)
    jsonContent=link.read()
    content=json.loads(jsonContent)
    rez=[]
    for el in content['data']['children']:
        rez.append(el['data']['title'])
    for title in rez:
        print title

def printCommentsXML(myUrl):
    link=urllib2.urlopen(myUrl)
    tree=ET.parse(link)
    root=tree.getroot()
    for child in root.iter():
        if 'title' in child.tag:
            print child.text

printCommentsXML("https://www.reddit.com/r/pythoncoding/.xml")