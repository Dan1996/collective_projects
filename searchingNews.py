import urllib2
from bs4 import BeautifulSoup
import re

def getSoupFromUrl(myUrl):
    htmlContent = urllib2.urlopen(myUrl)
    soup = BeautifulSoup(htmlContent, 'html.parser')
    soup.prettify()
    return soup

def getLinks(soup,nrLinks):
    linksList = soup.find_all('a')
    rez = []
    nr = 0
    for link in linksList:
        a = link.get('href')
        if re.search('[dD][nN][aA]', a):
            nr += 1
            rez.append(a)
            if nr==nrLinks:
                break
    return rez

def printText(nSoup):
    for line in nSoup.find_all('div'):
        if line.get('id')=='articleContent':
            print line.getText()

def printContentLink(rez):
    for link in rez:
        nSoup=getSoupFromUrl(link)
        printText(nSoup)
        print

def printNews(myUrl,nrLinks):
    soup=getSoupFromUrl(myUrl)
    rez=getLinks(soup,nrLinks)
    printContentLink(rez)


printNews('http://www.hotnews.ro',2)
