def splits(myString):
    l=[]
    for i in xrange(len(myString)+1):
        l.append((myString[:i],myString[i:]))
    return l

#transpozitii
#[('', 'abc'), ('a', 'bc'), ('ab', 'c'), ('abc', '')]
def transpozitii(myString):
    l=[]
    mySplits=splits(myString)
    mySplits.pop(0)
    mySplits.pop(-1)
    for el in mySplits:
        for i,char in enumerate(el[1]):
            l.append((char+el[0]+el[1][i+1:]))
            print char , el[0], el[1][i+1:]
    print l
    return l

transpozitii('abcd')
