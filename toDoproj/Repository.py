import domain
import json

class Repo(object):
    def __init__(self):
        self.rep=[]
        self.load()

    def load(self):
        f=None
        try:
            f=open('/home/dan/collective_projects/toDoproj/date')
            for line in f:
                self.rep.append(self.from_json(line))
        finally:
            if f is not None:
                f.close()

    def from_json(self,line):
        td=json.loads(line)
        if td['stare']=='True':
            stare=True
        else:
            stare=False
        tdCr=domain.toDo(td['id'],td['message'],td['time'],stare)
        return tdCr

    def to_json(self,td):
        newJson={}
        newJson['message']=td.msg
        newJson['id']=td.idT
        newJson['stare']=td.stare
        newJson['time']=td.data

    def save(self):
        f=open('/home/dan/collective_projects/toDoproj/date','w')
        for td in self.rep:
            f.write(self.to_json(td)+'\n')

    def generateID(self):
        idList = self.rep.keys()
        idList.sort()
        idT = 0
        for i, el in enumerate(idList):
            if i != el:
                idT = i
        if idT == 0:
            idT = len(idList)
        return idT

    def add(self,msg):
        idT=self.generateID()
        td=domain.toDo(idT,msg)
        self.rep.append(td)
        self.save()


#import sqlite3
#sqlite3.connect('lal.db')
#c=conn.cursor()
#c.execute('create table(id INTEGER,message TEXT)')
#conn.comit()
#conn.close()
#sqlitemanman lal.db